import os
import shutil
import datetime

srcfile = 'testfile.txt'
destfile = 'testdirectory/destfile.txt'
dirname = '/Volumes/VOL/Stf_H_Drv/moneill/Pleiad'

srcpath = os.path.join(dirname, srcfile)
destpath = os.path.join(dirname, destfile)

print os.path.isfile(srcpath)

shutil.copy2(srcpath, destpath)

print os.path.isfile(destpath)
# print os.path.samefile(srcfile, destfile)


# this doesn't go here; it needs to be a function that goes elsewhere
def rename(filename):
   goodDate = '01-' + filename

   verifiedGoodDate = datetime.datetime.strptime(goodDate, '%d-%m-%Y')

   fixedDate = verifiedGoodDate.strftime('%m-%d-%Y')

   print fixedDate

rename('10-1989')
