import os
import datetime

from os.path import join

# ElementTree is one way to parse XML; including it here

import xml.etree.ElementTree as ET

# xmltodict is another way to parse XML; including it here
# import xmltodict

# creating a dictionary to use with xmltodict
XMLfiles = []

# testing directory on desktop (one year)
dirs_to_crawl = '/Users/MeganOneil/Desktop/1940'

# testing directory with xml file (inside the directory above)
dirname = '/Users/MeganOneil/Desktop/1940/ingest_MeganOneil_20150216-113217/folder_00000/01-12-1940.xml'

# def crawl_for_exceptions(self):
#    if os.path.exists(dirs_to_crawl):   
#       exceptions_list = []
#       validated_names_list = []
#       for (dirname, dirs, files) in os.walk(dirname):
#          for filename in files:
#             if filename.endswith('.xml'):
#                XMLfiles.append(filename)
#       print 'Summary Section: '
#       print 'Successes: '
#       print len(validated_names_list)
#       print ''
#       print 'Exceptions: '
#       print len(exceptions_list)
#       for fname in exceptions_list: 
#          print fname
#    else:
#       print 'It ain\'nt there, kid.'

# def reformatXML(self):
#    # need to find a way to recursively feed file names to ET.parse()
#    cleanXML = ET.parse(dirname)
#    root = cleanXML.getroot()
#    iterator = root.getiterator()
#    for body in iterator:
#         body.replace("<dcvalue element=\"", "<dc:")
#         stringNoQualifier = body.find("\" qualifier=\"none\"")
#         if stringNoQualifier.exists:
#             body.replace("\" qualifier=\"none\"", "")
#         else:
#             body.replace("\" qualifier=\"", ".")
#             body.replace("</dcvalue>", "</dc>")
#         cleanXML.write(dirname)


def reformatXML(self):
    import xml.etree.ElementTree as ET
    tree = ET.parse(dirname)
    root = tree.getroot()
    for leadBracket in root.getiterator():
        if leadBracket.tag:
            leadBracket.tag = leadBracket.tag.replace("dcvalue element=\"", "dc:")
            leadBracket.tag = leadBracket.tag.replace("qualifier=\"", ":")
            print "tags: head"

    for endBracket in root.getiterator("</dcvalue>"):
        if endBracket.tail:
            endBracket.tail = endBracket.tail.replace("dcvalue", "/dc")
            print "tags: tail"
    leadBracket.set('updated', 'yes')
    endBracket.set('updated', 'yes')
    tree.write(dirname)

# I'd prefer to separate updating and re-writing the file if I can
# def replaceFile():
   # for file in dirname:
    #  cleanXML.write(dirname)

# crawl_for_exceptions(dirname)
# replaceFile(dirname)
reformatXML(dirname)
