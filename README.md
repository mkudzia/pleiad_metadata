# README #

All you'd need to do to get started with this is to have a machine on which you can run Python, and have a folder of PDF files you could run the program against. 

### What is this repository for? ###

* I'm trying to extract metadata from some files, and append some additional metadata. Eventually I want to have a MARCXML file for each PDF file.
* Version 0.1

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* moneill@albion.edu